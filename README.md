![ ](https://gitlab.com/Tacitus_Games/Chain_Reaction/raw/master/ChainReaction/Background.png)

# Chain Reaction

## About

This is a small game I made for Windows Phone 8 several years ago. I have made
some modifications to allow it to work on desktop. To play you must click tiles
to change their colour as well as the colour of neighbouring tiles. Colours
cycle through a spectrum and white tiles cannot be clicked. You win the game by
making a trail from the centre tile to the outer arrow of the same colour with a
trail of tiles of that colour. e.g. if the centre tile is green make a trail of
green tile to the green arrow. The more tiles are connected to this trail when
you complete it the higher your score but the more clicks it take the lower your
score.

----

## Examples

![Gameplay Video](https://gitlab.com/Tacitus_Games/Chain_Reaction/raw/release/assets/examples/Chain_Reaction_Gameplay_720p.mp4)

![ ](https://gitlab.com/Tacitus_Games/Chain_Reaction/raw/release/assets/examples/Capture_1.png)  

----
## [Downloads](https://gitlab.com/Tacitus_Games/Chain_Reaction/tags?utf8=%E2%9C%93&search=Release)
 * **Latest:** [ChainReaction.Win32.zip](https://gitlab.com/Tacitus_Games/Chain_Reaction/raw/Releases/Releases/ChainReaction.Win32.zip)

## Contact Info  

| Contact  | URL                                                 |
|----------|-----------------------------------------------------|
| Website  | [Tacitus.Games](https://tacitus.games)              |
| Mastodon | [@tacitus](https://mastodon.gamedev.place/@tacitus) |
