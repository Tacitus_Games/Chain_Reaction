/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using System.Threading;

#endregion

namespace Chain_Reaction
{
    public class Tools
    {
        public static float DistanceBetween(Point p1, Point p2)
        {
            return (float)Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
        }

        public static float getRotation(Point p, Point centre)
        {
            if (p.X == centre.X && p.Y > centre.Y) return 270f;
            if (p.X == centre.X && p.Y < centre.Y) return 90f;
            if (p.Y == centre.Y && p.X > centre.X) return 180f;
            if (p.Y == centre.Y && p.X <= centre.X) return 0f;

            p = new Point(p.X - centre.X, p.Y - centre.Y);
            p.Y *= -1;

            float angleA = 0;
            float angleTheta = 0;
            centre = Point.Zero;
            float radius = DistanceBetween(p, centre);
            angleA = RadiansToDegrees((float)Math.Atan((p.X / radius) / (p.Y / radius)));
            
            if(p.X > centre.X && p.Y > centre.Y)
            {
                angleTheta = angleA;
            }
            else if(p.X < centre.X && p.Y > centre.Y)
            {
                angleTheta = 180 - angleA;
            }
            else if (p.X < centre.X && p.Y < centre.Y)
            {
                angleTheta = 180 + angleA;
            }
            else
            {
                angleTheta = 360 - angleA;
            }

            return angleTheta;
        }

        public static Point RotateTo(Point p, Point centre, float rotation)
        {
            rotation = rotation % 360;

            if (rotation == getRotation(p, centre)) return p;

            float radius = DistanceBetween(p, centre);

            if (rotation == 0) return new Point(centre.X + (int)radius, centre.Y);
            if (rotation == 90) return new Point(centre.X, centre.Y - (int)radius);
            if (rotation == 180) return new Point(centre.X - (int)radius, centre.Y);
            if (rotation == 270) return new Point(centre.X, centre.Y + (int)radius);

            float angleA;

            if (rotation < 90)
            {
                angleA = rotation;
            }
            else if (rotation < 180)
            {
                angleA = 180 - rotation;
            }
            else if (rotation < 270)
            {
                angleA = 180 + rotation;
            }
            else
            {
                angleA = 360 - rotation;
            }

            //p = centre;
            int x, y;

            x = centre.X + (int)(((float)Math.Cos(DegreesToRadians(angleA))) * radius);
            y = centre.Y + (int)(((float)Math.Sin(DegreesToRadians(angleA))) * radius) * (-1);

           /* 
            p.X = (int)Math.Cos(DegreesToRadians(angleA));
            p.Y = (int)Math.sin(DegreesToRadians(angleA));
            p.Y *= -1;
            p.X *= (int)radius;
            p.Y *= (int)radius;
            p.X += centre.X;
            p.Y += centre.Y;
            */
            //p = new Point(centre.X + (int)(RadiansToDegrees((float)Math.Cos(DegreesToRadians(angleA))) * radius), centre.Y + (int)(RadiansToDegrees((float)Math.Sin(DegreesToRadians(angleA))) * radius * -1));

            p = new Point(x, y);

            return p;
        }

        public static Point RotateBy(Point p, Point centre, float rotationAmount)
        {
            return RotateTo(p, centre, (getRotation(p, centre) + rotationAmount));
        }

        public static float RadiansToDegrees(float radians) { return ((radians * 180) / (float)Math.PI); }
        public static float DegreesToRadians(float degrees) { return ((degrees * (float)Math.PI) / 180); }

        /// <summary>
        /// Delays for  number of milliseconds
        /// </summary>
        /// <param name="waitTime">The amount of milliseconds to delay for</param>
        public static void Wait(int waitTime)
        {
            
            
        }


        /// <summary>
        /// Transforms an array of bools into an array of bytes
        /// </summary>
        /// <param name="bits">An array of bools representing the bits in an array of byte, True == 1 and False == 0.</param>
        /// <param name="bytes">returns an array of bytes with the appropriate bits subtituted with the bits passed in</param>
        public static void BitsToBytes(bool[] bits, out byte[] bytes)
        {
            bytes = new byte[ ( bits.Length + ( 8 - bits.Length % 8 ) ) / 8 ];

            for (int i = 0; i < bits.Length; i++)
            {
                    if (bits[i])
                    {
                        bytes[i/8] += (byte)Math.Pow(2, (i + ( 8 - i % 8 ) ) / 8);
                    }
            }
            //bytes = new byte[] { 2 };
        }

        /// <summary>
        /// Transforms an array of bytes into an array of bools
        /// </summary>
        /// <param name="bits">returns an array of bools representing the bits in bytes, True == 1 and False == 0.</param>
        /// <param name="bytes">An array of bytes to be translated</param>
        public static void BytesToBits(byte[] bytes, out bool[] bits)
        {
            bits = new bool[bytes.Length * 8];
            //bits = new bool[3];
            int j = bits.Length;

            for (int k = bytes.Length - 1; k >= 0; k-- )
            {
                for (int i = 0; i < 8; i++)
                {
                    j--;

                    bits[j] = ( ( bytes[k] % 2 ) == 1);

                    bytes[k] /= 2;
                }
            }
        }

    }
}
