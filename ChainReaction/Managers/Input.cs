/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

#endregion

namespace Chain_Reaction.Managers
{
    public class Input
    {
		#region Fields

//		private static TouchCollection currTouchLocations;
//		private static TouchCollection prevTouchLocations;

		private static MouseState currTouchLocations;
		private static MouseState prevTouchLocations;

		#endregion

		#region Properties

//		public static TouchCollection touchCollection { get { return currTouchLocations; } }

        #endregion

        #region Constructors

        public static void Initialise()
        {
            currTouchLocations = Mouse.GetState();
            prevTouchLocations = currTouchLocations;
        }

        #endregion

        #region Generic Methods

        public static void Update()
        {
            //if (TouchPanel.GetState().Equals(currTouchLocations)) return;

            prevTouchLocations = currTouchLocations;
            currTouchLocations = Mouse.GetState();
        }

        #endregion

        #region Specific Methods

        public static bool IsPressed
        {
            get
            {
				return currTouchLocations.LeftButton == ButtonState.Pressed && prevTouchLocations.LeftButton == ButtonState.Released;
/** /           if (currTouchLocations.Count == 0) return false;
                else
                {
                    return (currTouchLocations[0].State == TouchLocationState.Pressed || currTouchLocations[0].State == TouchLocationState.Moved);
                }/**/
            }
        }

        public static bool IsReleased
        {
            get
            {
				return prevTouchLocations.LeftButton == ButtonState.Pressed && currTouchLocations.LeftButton == ButtonState.Released;
				/** /
				if (currTouchLocations.Count == 0) return false;
                else
                {
                    return (currTouchLocations[0].State == TouchLocationState.Released);
                }
				/**/
			}
		}

        public static Point TouchLocation
        {
            get
            {
				return new Point((int)currTouchLocations.Position.X, (int)currTouchLocations.Position.Y);
				/** /
				if (currTouchLocations.Count == 0) return new Point(-100, -100);
                else return new Point((int)currTouchLocations[0].Position.X, (int)currTouchLocations[0].Position.Y);
				/**/
			}
		}

        #endregion

    }
}
