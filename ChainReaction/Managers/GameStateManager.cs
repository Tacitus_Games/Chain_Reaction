/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Chain_Reaction.Scenes;

#endregion


namespace Chain_Reaction.Managers
{
    public class GameStateManager
    {

        #region Feilds

        public enum State : byte {MainMenu, GameScene, PauseScreen, ScoreScene, HighScoreScreen, Terminate };
        private State state;
        private MainMenuScene mainMenu;
        private GameScene gameScene;
        private PauseScreen pauseScreen;
        private ScoreScreen scoreScreen;
        private bool loading;

        #endregion

        #region Properties

        public State gameState { get { return state; } }

        #endregion


        #region Constructors

        public GameStateManager()
        {
            state = State.MainMenu;
            loading = true;
        }

        #endregion

        #region Generic Methods

        public void Update()
        {
            if(loading)return;

            switch (state)
            {
                case State.MainMenu: if (mainMenu == null) { mainMenu = new MainMenuScene(); loading = true; }
                    if (loading) return;
                    mainMenu.Update();
                    if (mainMenu.started) { mainMenu.Flush(); state = State.GameScene; gameScene = new GameScene(); loading = true; }
                    //else if (mainMenu.exited) { state = State.Terminate; }
                    break;
                case State.GameScene: if (gameScene == null){ gameScene = new GameScene(); loading = true;}
                    if (loading) return;
                    gameScene.Update();
                    if (gameScene.paused) { state = State.PauseScreen; loading = true; }
                    if (gameScene.finished) { state = State.ScoreScene; loading = true; }
                    break;
                case State.PauseScreen: if (pauseScreen == null) { pauseScreen = new PauseScreen(); loading = true; }
                    if (loading) return;
                    pauseScreen.Update();
                    if (pauseScreen.resumed) { pauseScreen.Flush(); gameScene.Flush(); state = State.GameScene; }
                    else if (pauseScreen.exited) { pauseScreen.Flush(); state = State.MainMenu; loading = true; }
                    break;
                case State.ScoreScene: if (scoreScreen == null) { scoreScreen = new ScoreScreen(); loading = true; }
                    if (loading) return;
                    int points, taps, highestChain;
                    gameScene.getStats(out taps, out points, out highestChain);
                    scoreScreen.Update(taps, points, highestChain);
                    if (scoreScreen.exited) { scoreScreen.Flush(); scoreScreen.NewRandom(); state = State.MainMenu; loading = true; }
                    break;
            }
        }

        public void Draw()
        {
            if (loading)
            {
                string s = "LOADING...";
                Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2(Statics.centre.X - Statics.spriteFont.MeasureString(s).X / 2, Statics.centre.Y), Color.YellowGreen);
                loading = false;
                return;
            }

            switch (state)
            {
                case State.MainMenu: mainMenu.Draw();
                    break;
                case State.GameScene: gameScene.Draw();
                    break;
                case State.PauseScreen:
                    gameScene.Draw();
                    pauseScreen.Draw();
                    break;
                case State.ScoreScene: scoreScreen.Draw();
                    break;
            }
        }

        #endregion

    }
}
