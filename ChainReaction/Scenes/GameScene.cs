/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using Chain_Reaction.Objects;
using Chain_Reaction.Managers;

#endregion

namespace Chain_Reaction.Scenes
{
    public class GameScene
    {

        #region Fields

        private List<Tile> tiles;
        private Button[] arrows;
        private Jewel jewel;
        private int diameter = 9;
        private Vector2 offset = new Vector2(5, 5);
        private bool chain = false;
        private byte toChain;
        private int numberOfClicks = 0, thisChainNumber, score = 0, highestChain;
        private Button menu;

        #endregion

        #region Properties

        public bool finished { get { return (jewel.colour == 0); } }

        public bool paused { get { return menu.pressed; } }

        #endregion

        #region Constructors

        public GameScene()
        {
            tiles = new List<Tile>();
            Populate();

            arrows = new Button[6];
            arrows[0] = new Button(Button.Type.arrow, new Point((int)(tiles[0].centre.X - ((Statics.tileTexture.Width / 2) + Statics.arrowTex.Width)), (int)(tiles[0].centre.Y - (Statics.arrowTex.Height / 2))), new byte[] { 0, 0, 255 }, false);
            arrows[1] = new Button(Button.Type.arrow, new Point((int)Statics.centre.X * 2 - arrows[0].position.X - Statics.arrowTex.Width, arrows[0].position.Y), new byte[] { 0, 255, 0 }, true);
            arrows[2] = new Button(Button.Type.arrow, new Point((int)(Statics.centre.X + (((float)diameter / 2)*(Statics.tileTexture.Width + offset.X))) , (int)(Statics.centre.Y - Statics.arrowTex.Height / 2)), new byte[] { 0, 255, 255 }, true);
            arrows[3] = new Button(Button.Type.arrow, new Point(arrows[1].position.X, (int)(Statics.centre.Y * 2 - arrows[1].position.Y) - Statics.arrowTex.Height), new byte[] { 255, 0, 0 }, true);
            arrows[4] = new Button(Button.Type.arrow, new Point(arrows[0].position.X, arrows[3].position.Y), new byte[] { 255, 0, 255 }, false);
            arrows[5] = new Button(Button.Type.arrow, new Point((int)(Statics.centre.X * 2 - arrows[2].position.X - Statics.arrowTex.Width), arrows[2].position.Y), new byte[] { 255, 255, 0 }, false);

            menu = new Button(Button.Type.circle,"pause",Color.YellowGreen, new Point(Statics.screenWidth - Statics.circleTex.Width,  0), new byte[] { 0, 0, 255 }, false);

            byte num = (byte)Statics.random.Next(6);

            while (num > 0)
            {
                tiles[Statics.random.Next(tiles.Count)].colour = 0;
                num--;
            }

            SetGroups();

            Offset(new Point(50, 0));

            highestChain = 0;
            thisChainNumber = 0;
        }

        #endregion

        #region Generic methods

        public void Update()
        {
            menu.Update();

            int highlight = -1;
            if (Tools.DistanceBetween(Input.TouchLocation, Statics.centre) <= (Statics.tileTexture.Width+offset.X)*diameter)
            {
                for (int i = 0; i < tiles.Count; i++ )
                {
                    if (Tools.DistanceBetween(Input.TouchLocation, tiles[i].centre) <= Statics.tileTexture.Width / 2)
                    {
                        if (Input.IsPressed) highlight = i;
                        else if (Input.IsReleased) tap(i);
                    }
                }
            }
            foreach (Tile t in tiles) { t.highlight = false; }
            if (highlight != -1) tiles[highlight].highlight = true;

            foreach (Button b in arrows)
            {
                b.Update();
            }

            chain = false;
            for (byte i = 0; i < arrows.Length; i++ )
            {
                if (arrows[i].pressed)
                {
                    chain = true;
                    toChain = i;
                    arrows[i].pressed = false;
                }
            }
        }

        public void Draw()
        {
            menu.Draw();

            jewel.Draw();

            foreach (Button b in arrows)
            {
                b.Draw();
            }

            foreach (Tile tile in tiles)
            {
                tile.Draw();
            }

            if (chain)
            {
                Chain(arrows[toChain].colour, new List<int>() { arrows[toChain].closestTile });
            }
            thisChainNumber = 0;

            string clicksString = "Taps: ";
            clicksString += numberOfClicks;
            Statics.spriteBatch.DrawString(Statics.spriteFont, clicksString, new Vector2(0, 0), Color.Yellow);

            string scoreString = "Score: ";
            scoreString += score;
            Statics.spriteBatch.DrawString(Statics.spriteFont, scoreString, new Vector2(0, 0 + Statics.spriteFont.MeasureString(clicksString).Y), Color.Yellow);
        }

        #endregion

        #region Specific Methods

        private void Offset(Point offsetAmount)
        {
            foreach (Button b in arrows)
            {
                b.position = new Point(b.position.X + offsetAmount.X, b.position.Y + offsetAmount.Y);
            }

            foreach (Tile t in tiles)
            {
                t.centre = new Point(t.centre.X + offsetAmount.X, t.centre.Y + offsetAmount.Y);
            }

            jewel.centre = new Point(jewel.centre.X + offsetAmount.X, jewel.centre.Y + offsetAmount.Y);
        }

        private void Populate()
        {
            jewel = new Jewel(RandomColour(), new Point(Statics.centre.X, Statics.centre.Y));
            for (int i = 0; i < diameter; i++)
            {
                int amount = diameter - Math.Abs(i - (diameter / 2));
                int yPos = (int)(Statics.centre.Y + (Statics.tileTexture.Height * (0.75) + offset.Y) * (i - diameter / 2));
                for (int j = 0; j < amount; j++)
                {
                    int xPos = (int)(Statics.centre.X + (Statics.tileTexture.Width + offset.X) * (j - amount / 2) + (Statics.tileTexture.Width / 2 + offset.X / 2) * (i % 2));
                    if (Statics.centre != new Point(xPos, yPos)) tiles.Add(new Tile(RandomColour(), new Point(xPos, yPos)));
                }
            }
        }

        private void SetGroups()
        {
            List<int> l;
            foreach (Tile t in tiles)
            {
                l = new List<int>();
                for (int i = 0; i < tiles.Count; i++)
                {
                    if (Tools.DistanceBetween(t.centre, tiles[i].centre) < (Statics.tileTexture.Width + offset.X + 5) && t.centre != tiles[i].centre)
                    {
                        l.Add(i);
                    }
                }
                t.closestTiles = l.ToArray();
            }

            foreach (Button b in arrows)
            {
                int closest = 0;

                for (int i = 1; i < tiles.Count; i++)
                {
                    if (Tools.DistanceBetween(b.point, tiles[i].centre) < Tools.DistanceBetween(b.point, tiles[closest].centre))
                    {
                        closest = i;
                    }
                }

                b.closestTile = closest;
            }

                l = new List<int>();
                for (int i = 0; i < tiles.Count; i++)
                {
                    if (Tools.DistanceBetween(jewel.centre, tiles[i].centre) < (Statics.tileTexture.Width + offset.X + 5))
                    {
                        l.Add(i);
                    }
                }
                jewel.closestTiles = l.ToArray();

                foreach (Button b in arrows)
                {
                    while (b.colour == jewel.colour && tiles[b.closestTile].colour == 0)
                    {
                        tiles[b.closestTile].colour = RandomColour();
                    }
                }
        }

        private byte RandomColour()
        {
            byte rgb;

            do
            {
                rgb = (byte)Statics.random.Next(1, 7);
            }while (7 == rgb);

            return rgb;
        }

        private void tap(int i)
        {
            if (tiles[i].colour == 0 || tiles[i].colour == 7) return;
            numberOfClicks++;
            tiles[i].ChangeColour();

            foreach (int n in tiles[i].closestTiles)
            {
                tiles[n].ChangeColour();
            }
        }

        private void Chain(byte mode, List<int> current)
        {
            chain = false;
            thisChainNumber++;
            highestChain = thisChainNumber + 1;
            List<int> next = new List<int>();

            foreach (int t in current)
            {
                if (tiles[t].colour == mode)
                {
                    foreach (int ct in tiles[t].closestTiles)
                    {
                        if (tiles[ct].colour == mode) next.Add(ct);
                        else tiles[ct].ChangeColour();
                    }
                    tiles[t].colour = 0;
                    score += Statics.random.Next(150, 200) * thisChainNumber;
                    foreach (int i in jewel.closestTiles)
                    {
                        if (jewel.colour == mode && i == t)
                        {
                            jewel.colour = 0;
                        }

                    }
                }
            }

            Draw();

            if(next.Count != 0) Chain(mode, next);
        }

        public void Flush()
        {
            menu.Flush();
        }

        public void getStats(out int clicks, out int points, out int largestChain)
        {
            clicks = numberOfClicks;
            points = score;
            largestChain = highestChain;
        }

        #endregion
    }
}