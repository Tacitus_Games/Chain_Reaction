/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Chain_Reaction.Objects;

#endregion


namespace Chain_Reaction.Scenes
{
    public class MainMenuScene// : PauseScreen
    {

        #region Feilds

        //private float startRotation = 90, exitRotation = 180;
        private Button start;

        #endregion

        #region Properties

        public bool started { get { return start.pressed; } }

        #endregion

        #region Constructors

        public MainMenuScene()
        {
            //choice = Choice.resume;

            start = new Button(Button.Type.circle, "Start", Color.Black, new Point((int)Statics.centre.X, (int)Statics.centre.Y), new byte[] { 0, 255, 0 }, false);
            //exit = new Button(Button.Type.circle, "Quit", Color.Black, new Point((int)Statics.centre.X, (int)Statics.centre.Y + 150), new byte[] { 255, 0, 0 }, false);
            //yes = new Button(Button.Type.circle, "YES", Color.Black, new Point((int)(Statics.centre.X - Statics.circleTex.Width - 5), (int)Statics.centre.Y), new byte[] { 0, 255, 0 }, false);
            //no = new Button(Button.Type.circle, "NO", Color.Black, new Point((int)Statics.centre.X + 5, (int)Statics.centre.Y), new byte[] { 255, 0, 0 }, false);

            start.centre = (start.position);
            //exit.centre = (exit.position);
        }

        #endregion

        #region Generic Methods

        public void Update()
        {
            start.Update();
            /*base.Update();
            /////////////////
            if (choice == Choice.resume && false)
            {
                resume.centre = new Point((int)Statics.centre.X, (int)Statics.centre.Y - 150);
                exit.centre = new Point((int)Statics.centre.X, (int)Statics.centre.Y + 150);

                startRotation = (startRotation + 1) % 360;
                exitRotation = (exitRotation + 1) % 360;

                resume.centre = Tools.RotateTo(resume.centre, Statics.centre, 100);
                exit.centre = Tools.RotateTo(exit.centre, Statics.centre, 200);
            }*/
        }

        public void Draw()
        {
            start.Draw();
            /*exit.Draw();

            if (choice != Choice.resume)
            {
                string s = "Are you sure?";
                Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2(Statics.centre.X - Statics.spriteFont.MeasureString(s).X / 2, yes.rect.Location.Y - Statics.spriteFont.MeasureString(s).Y), Color.White);

                yes.Draw();
                no.Draw();
            }*/
        }

        #endregion

        #region Specific Methods

        public void Flush()
        {
            start.Flush();
            //exit.Flush();
            //yes.Flush();
            //no.Flush();
            //choice = Choice.resume;
        }

        #endregion


    }
}
