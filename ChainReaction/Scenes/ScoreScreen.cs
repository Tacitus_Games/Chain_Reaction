/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */


#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Chain_Reaction.Objects;

#endregion

namespace Chain_Reaction.Scenes
{
    public class ScoreScreen
    {

        #region Fields

        private float taps, points, largestChain;
        private int r;
        private Button exit;

        #endregion

        #region Properties

        public bool exited { get { return exit.pressed; } }

        #endregion

        #region Constructors

        public ScoreScreen()
        {
            NewRandom();
            exit = new Button(Button.Type.rectangle, "Exit", Color.Black,  new Point(Statics.centre.X - Statics.rectangleTex.Width / 2, Statics.screenHeight - Statics.rectangleTex.Height), new byte[] { 255, 255, 0 }, false);
        }

        #endregion

        #region Generic Methods

        public void Update(int taps, int points, int highestChain)
        {
            this.taps = taps;
            this.points = points;
            largestChain = highestChain;

            exit.Update();
        }

        public void Draw()
        {
            float scale = 1;
            string s;

            exit.Draw();

            s = "Points: ";
            s += (int)points;
            Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2((float)(Statics.centre.X - Statics.spriteFont.MeasureString("Points: ").X), (float)(Statics.centre.Y - (2.5 * (Statics.spriteFont.MeasureString("Points: ").Y)))), Color.Yellow, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
            /*
            s = "Finish Bonus: ";
            s += (int)((points * points) - points);
            Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2((float)(Statics.centre.X - Statics.spriteFont.MeasureString("Finish Bonus: ").X), (float)(Statics.centre.Y - (2.5 * (Statics.spriteFont.MeasureString("Finish Bonus: ").Y)))), Color.Yellow, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
            */
            s = "Taps: ";
            s += (int)taps;
            Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2((float)(Statics.centre.X - Statics.spriteFont.MeasureString("Taps: ").X), (float)(Statics.centre.Y - (1.5 * (Statics.spriteFont.MeasureString("Taps: ").Y)))), Color.Yellow, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);

            s = "Subtotal: ";
            s += (int)((points) / taps);
            Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2((float)(Statics.centre.X - Statics.spriteFont.MeasureString("Subtotal: ").X), (float)(Statics.centre.Y - (0.5 * (Statics.spriteFont.MeasureString("Subtotal: ").Y)))), Color.Yellow, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
            /*
            s = "HighestChain: ";
            s += (int)(largestChain);
            Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2((float)(Statics.centre.X - Statics.spriteFont.MeasureString("HighestChain: ").X), (float)(Statics.centre.Y + (1 * (Statics.spriteFont.MeasureString("HighestChain: ").Y)))), Color.Yellow, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
            */
            s = "Bonus: ";
            s += (int)(largestChain * r);
            Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2((float)(Statics.centre.X - Statics.spriteFont.MeasureString("Bonus: ").X), (float)(Statics.centre.Y + (0.5 * (Statics.spriteFont.MeasureString("Bonus: ").Y)))), Color.Yellow, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);

            s = "Total: ";
            s += (int)((largestChain * r) + ((points) / taps));
            Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2((float)(Statics.centre.X - Statics.spriteFont.MeasureString("Total: ").X), (float)(Statics.centre.Y + (1.5 * (Statics.spriteFont.MeasureString("Total: ").Y)))), Color.Yellow, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
        }

        #endregion

        #region Specific Methods

        public void NewRandom()
        {
            r = Statics.random.Next(155, 200);
        }

        public void Flush()
        {
            exit.Flush();
        }

        #endregion
    }
}
