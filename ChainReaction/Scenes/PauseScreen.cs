/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Chain_Reaction.Objects;

#endregion


namespace Chain_Reaction.Scenes
{
    public class PauseScreen
    {

        #region Feilds

        protected Button resume, exit, yes, no;
        protected enum Choice : byte { resume, exit };
        protected Choice choice;

        #endregion

        #region Properties

        public bool resumed { get { return resume.pressed; } }

        public bool exited { get { return (choice == Choice.exit && yes.pressed); } }

        #endregion

        #region Constructors

        public PauseScreen()
        {
            choice = Choice.resume;

            resume = new Button(Button.Type.rectangle, "Resume", Color.Black, new Point((int)(Statics.centre.X - Statics.rectangleTex.Width - 5), (int)Statics.centre.Y), new byte[] { 0, 255, 0 }, false);
            exit = new Button(Button.Type.rectangle, "Exit", Color.Black, new Point((int)Statics.centre.X + 5, (int)Statics.centre.Y), new byte[] { 255, 0, 0 }, false);
            yes = new Button(Button.Type.rectangle, "YES", Color.Black, new Point((int)(Statics.centre.X - Statics.rectangleTex.Width - 5), (int)Statics.centre.Y - 50), new byte[] { 0, 255, 0 }, false);
            no = new Button(Button.Type.rectangle, "NO", Color.Black, new Point((int)Statics.centre.X + 5, (int)Statics.centre.Y - 50), new byte[] { 255, 0, 0 }, false);
        }

        #endregion

        #region Generic Methods

        public void Update()
        {
            if (exit.pressed) choice = Choice.exit;

            if (no.pressed)
            {
                choice = Choice.resume;
                Flush();
            }

            if (choice == Choice.resume)
            {
                resume.Update();
                exit.Update();
            }

            else
            {
                yes.Update();
                no.Update();
            }
        }

        public void Draw()
        {
            Statics.spriteBatch.Draw(Statics.pixelTex, Vector2.Zero, null, new Color(64, 64, 64, 100), 0f, Vector2.Zero, 1000, SpriteEffects.None, 1);

            resume.Draw();
            exit.Draw();

            if (choice != Choice.resume)
            {
                Statics.spriteBatch.Draw(Statics.pixelTex, Vector2.Zero, null, new Color(64, 64, 64, 100), 0f, Vector2.Zero, 1000, SpriteEffects.None, 1);
                string s = "Are you sure?";
                Statics.spriteBatch.DrawString(Statics.spriteFont, s, new Vector2(Statics.centre.X - Statics.spriteFont.MeasureString(s).X / 2, yes.rect.Location.Y - Statics.spriteFont.MeasureString(s).Y), Color.Black);

                yes.Draw();
                no.Draw();
            }
        }

        #endregion

        #region Specific Methods

        public void Flush()
        {
            resume.Flush();
            exit.Flush();
            yes.Flush();
            no.Flush();
        }

        #endregion


    }
}
