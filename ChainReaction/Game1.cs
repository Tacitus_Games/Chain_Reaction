/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using Chain_Reaction.Scenes;
using Chain_Reaction.Managers;

#endregion

namespace Chain_Reaction
{
    public class Game1 : Game
    {

        #region Fields

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GameStateManager GSM;

        #endregion

        #region Constructors

        public Game1()
        {
            #region Setup

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            //graphics.PreferredBackBufferWidth = 1080;
            //graphics.PreferredBackBufferHeight = 720;

            graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;

            TargetElapsedTime = TimeSpan.FromTicks(333333);

            InactiveSleepTime = TimeSpan.FromSeconds(1);

            #endregion

            Input.Initialise();

            GSM = new GameStateManager();
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Statics.Load(Content.Load<SpriteFont>("SpriteFont1"), spriteBatch, Content.Load<Texture2D>("Rectangle Button"), Content.Load<Texture2D>("Circle Button"), Content.Load<Texture2D>("Pixel"), Content.Load<Texture2D>("Hex Tile"), Content.Load<Texture2D>("Tile Highlight"), Content.Load<Texture2D>("Hex Jewel"), Content.Load<Texture2D>("Arrow"), GraphicsDevice.Viewport.Bounds.Height, GraphicsDevice.Viewport.Bounds.Width);
        }

        #endregion

        #region Unload Content

        protected override void UnloadContent()
        {
        }

        #endregion

        #region Generic Methods

        protected override void Update(GameTime gameTime)
        {
            Input.Update();
            Statics.Update(gameTime);

            GSM.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(0, 0, 0));

            Statics.spriteBatch.Begin();

            GSM.Draw();
			Statics.spriteBatch.Draw(Content.Load<Texture2D>("CrossHairs"), new Vector2( Input.TouchLocation.X-25, Input.TouchLocation.Y-25), null, new Color(0.5f, 0.5f, 0.5f, 1.0f), 0, Vector2.Zero, 0.5f, SpriteEffects.FlipHorizontally, 1);

			Statics.spriteBatch.End();

            /** /spriteBatch.Begin();
            string s = "";
            s += Input.TouchLocation.X;
            s += ", ";
            s += Input.TouchLocation.Y;
            s += ", ";
            s += GraphicsDevice.Viewport.Height;
            spriteBatch.DrawString(Statics.spriteFont, s, Vector2.Zero, Color.Red);
            spriteBatch.End();/**/

            base.Draw(gameTime);
        }

        #endregion

    }
}