/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Chain_Reaction.Managers;

#endregion

namespace Chain_Reaction.Objects
{
    public class Button
    {

        #region Fields

        public enum Type : byte { arrow, rectangle, circle };
        private Type type;
        private Point pos;
        private bool down;
        private byte[] rgb;
        private bool flipped;
        private int tile;
        private string text = "";
        private Color textColour = Color.White;

        #endregion

        #region Properties

        public Rectangle rect
        {
            get
            {
                Rectangle r = new Rectangle((int)pos.X, (int)pos.Y, 1, 1);

                switch (type)
                {
                    case Type.arrow:
                        r.Width = Statics.arrowTex.Width;
                        r.Height = Statics.arrowTex.Height;
                        break;
                    case Type.rectangle:
                        r.Width = Statics.rectangleTex.Width;
                        r.Height = Statics.rectangleTex.Height;
                        break;
                    case Type.circle:
                        r.Width = Statics.circleTex.Width;
                        r.Height = Statics.circleTex.Height;
                        break;
                    default:
                        r.Width = 10;
                        r.Height = 10;
                        break;
                }

                return r;
            }
        }

        public byte colour
        {
            get
            {
                byte c = 0;

                for( byte i = 0; i < 3; i++)
                {
                    c += (byte)((((rgb[i]) > 0) ? 1 : 0) * Math.Pow(2, i));
                }
                
                return c;
            }
        }

        public Point point
        {
            get
            {
                Point v = new Point();
                v.Y = pos.Y + Statics.arrowTex.Height / 2;

                if (!flipped) v.X = pos.X + Statics.arrowTex.Width;
                else v.X = pos.X;

                return v;
            }
        }

        public bool pressed
        {
            get { return down; }
            set { down = value; }
        }

        public Point position { get { return pos; } set { pos = value; } }

        public Point centre
        {
            get { return rect.Center; }
            
            set
            {
                switch (type)
                {
                    case Type.arrow: pos = new Point(value.X - Statics.arrowTex.Width / 2, value.Y - Statics.arrowTex.Height / 2);
                        break;
                    case Type.rectangle: pos = new Point(value.X - Statics.rectangleTex.Width / 2, value.Y - Statics.rectangleTex.Height / 2);
                        break;
                    case Type.circle: pos = new Point(value.X - Statics.circleTex.Width / 2, value.Y - Statics.circleTex.Height / 2);
                        break;
                    default: pos = new Point(0, 0);
                        break;
                }
            }
        }

        public int closestTile
        {
            get { return tile; }
            set { tile = value; }
        }

        #endregion

        #region Constructors

        public Button(Type type, Point position, byte[] colour, bool flip)
        {
            this.type = type;
            pos = new Point(position.X, position.Y);
            rgb = colour;
            flipped = flip;
        }

        public Button(Type type, string text, Color textColour, Point position, byte[] colour, bool flip)
        {
            this.type = type;
            pos = new Point(position.X, position.Y);
            rgb = colour;
            flipped = flip;
            this.text = text;
            this.textColour = textColour;
        }

        #endregion

        #region Generic Methods

        public void Update()
        {

            for (int i = 0; i < rgb.Length; i++)
            {
                if (rgb[i] != 0) rgb[i] = 255;
            }

            if (Input.IsPressed && rect.Contains(new Point((int)Input.TouchLocation.X, (int)Input.TouchLocation.Y)))
            {
                for (int i = 0; i < rgb.Length; i++)
                {
                    if(rgb[i] != 0) rgb[i] = 100;
                }
            }
            else if (Input.IsReleased && rect.Contains(new Point((int)Input.TouchLocation.X, (int)Input.TouchLocation.Y)))
            {
                down = true;
            }
        }

        public void Draw()
        {
            Texture2D tex;

            switch (type)
            {
                case Type.arrow: tex = Statics.arrowTex;
                    break;
                case Type.rectangle: tex = Statics.rectangleTex;
                    break;
                case Type.circle: tex = Statics.circleTex;
                    break;
                default: tex = Statics.arrowTex;
                    break;
            }

            if (!flipped)
            {
                Statics.spriteBatch.Draw(tex, new Vector2(pos.X, pos.Y), new Color(rgb[0], rgb[1], rgb[2]));
            }
            else
            {
                Statics.spriteBatch.Draw(tex, new Vector2(pos.X, pos.Y), null, new Color(rgb[0], rgb[1], rgb[2]), 0, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 1);
            }

            Statics.spriteBatch.DrawString(Statics.spriteFont, text, new Vector2(rect.Center.X - Statics.spriteFont.MeasureString(text).X / 2, rect.Center.Y - Statics.spriteFont.MeasureString(text).Y / 2), textColour);
        }

        #endregion

        #region Spectific Methods

        public void Flush()
        {
            down = false;
        }

        #endregion

    }
}