/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

#endregion

namespace Chain_Reaction.Objects
{
    public class Jewel : Tile
    {

        #region Constructors

        public Jewel(byte rgb, Point centre)
            : base(rgb, centre)
        {

        }

        #endregion

        #region Generic Methods

        public void Draw()
        {
            if (mode == 0) return;
            Texture2D tex = Statics.jewelTex;
            bool[] rgb;
            Tools.BytesToBits(new byte[] { mode }, out rgb);
            Statics.spriteBatch.Draw(tex, new Vector2(pos.X - (tex.Width / 2 + tex.Width % 2), pos.Y - (tex.Height / 2 + tex.Height % 2)), new Color((float)(rgb[rgb.Length - 1] ? 1 : 0), (float)(rgb[rgb.Length - 2] ? 1 : 0), (float)(rgb[rgb.Length - 3] ? 1 : 0)));
        }

        #endregion

    }
}
