/*
 * Copyright © Tacitus 2018, all rights reserved
 * https://tacitus.games
 * https://twitter.com/TacitusGameDev
 */

#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

#endregion

namespace Chain_Reaction
{
    public class Statics
    {
        #region Private Fields

        private static int height, width;
        private static SpriteBatch sBatch;
        private static Texture2D tileTex, jTex, aTex, pixTex, hTex, rectTex, circTex;
        private static Random r;
        private static GameTime gt;
        private static SpriteFont sFont;

        #endregion

        #region Public Properties

        public static SpriteFont spriteFont { get { return sFont; } }

        public static Random random { get { return r; } }

        public static Point centre { get { return new Point(width / 2 + width % 2, height / 2 + height % 2); } }

        public static Texture2D tileTexture { get { return tileTex; } }

        public static Texture2D highlightTex { get { return hTex; } }

        public static Texture2D arrowTex { get { return aTex; } }

        public static Texture2D jewelTex { get { return jTex; } }

        public static Texture2D pixelTex { get { return pixTex; } }

        public static Texture2D rectangleTex { get { return rectTex; } }

        public static Texture2D circleTex { get { return circTex; } }

        public static int screenWidth { get { return width; } }

        public static int screenHeight { get { return height; } }

        public static SpriteBatch spriteBatch
        {
            get { return sBatch; }
        //    set { sBatch = value; }
        }

        public static GameTime gameTime { get { return gt; } }

        #endregion

        #region Generic Methods

        public static void Load(SpriteFont spriteFont, SpriteBatch spriteBatch, Texture2D rectangleTex, Texture2D circleTex, Texture2D pixelTex, Texture2D tile, Texture2D highlightTex, Texture2D jewelTex, Texture2D arrowTex, int screenHeight, int screenWidth)
        {
            r = new Random();

            sBatch = spriteBatch;
            width = screenWidth;
            height = screenHeight;
            tileTex = tile;
            hTex = highlightTex;
            jTex = jewelTex;
            aTex = arrowTex;
            pixTex = pixelTex;
            rectTex = rectangleTex;
            circTex = circleTex;

            sFont = spriteFont;
        }

        public static void Update(GameTime gameTime)
        {
            gt = gameTime;
            r = new Random();
        }

        #endregion

    }
}